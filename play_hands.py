# -*- coding: utf-8 -*-

from random import randrange
from random import shuffle
from newdeck import newdeck
from evaluate import hand_value
from evaluate import hand_vs_hand
from evaluate import card_single_char
from evaluate import convert_hand

def play_hands(n, strategy):
    results = []
    stand = 0
    hit = 1

    for x in range(0,n):
        deck = list(newdeck)
        shuffle(deck)
        
        dealerhand = []
        dealercardone = deck.pop()
        dealercardtwo = deck.pop()
        dealerhand.append(dealercardone)
        dealerhand.append(dealercardtwo)
        if hand_value(dealerhand) < 17:
            dealercardthree = deck.pop()
            dealerhand.append(dealercardthree)

        babycardone = deck.pop()
        babycardtwo = deck.pop()
        babyhand = convert_hand(babycardone, babycardtwo)
        baby_eval_hand = hand_value(babyhand)        

        if baby_eval_hand != 21:
            action = stand
            babycardone = card_single_char(babyhand[0])
            babycardtwo = card_single_char(babyhand[1])
            dealercardone = card_single_char(dealercardone)
            a = babycardone + babycardtwo
            asorted = ''.join(sorted(a))
            babybabydealer = asorted + dealercardone
            hitpercent = strategy.get_strategy_for_hand(babybabydealer)
            rand = randrange(0,100,1)
            if hitpercent > rand:
                action = hit
                babycardthree = deck.pop()
                babyhand.append(babycardthree)
            result = hand_vs_hand(dealerhand, babyhand)    
            results.append((babycardone, babycardtwo, dealercardone, action, result))
    return results
