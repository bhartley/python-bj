#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 15 17:42:02 2017

@author: ben
"""

import sys
from play_hands import play_hands
from strategy import Strategy

handstoplay = 100
if len(sys.argv) > 1:
    if sys.argv[1] == 'thousand':
        handstoplay = 1000
    elif sys.argv[1] == 'tenthousand':
        handstoplay = 10000

strategy = Strategy()
print('Playing ' + str(handstoplay) + ' hands')
hands = play_hands(handstoplay, strategy)
strategy.strategize(hands)

test = strategy.get_strategy_for_hand('003')
print(test)
