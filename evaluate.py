# -*- coding: utf-8 -*-

numbers = ['2','3','4','5','6','7','8','9']
faces = ['0','T','J','Q','K']
ace = 'A'

def hand_value(cards):
    total = 0
    for card in cards:
        if card in numbers:
            total = total + int(card)
        elif card in faces:
            total = total + 10
        elif card == ace:
            if total + 11 > 21:
               total = total + 1
            else:
                total = total + 11
    if total > 21:
        total = -1
    return total

def hand_vs_hand(dealerhand, babyhand):
    dealerhandvalue = hand_value(dealerhand)
    babyhandvalue = hand_value(babyhand)
    if dealerhandvalue > babyhandvalue:
        return '0'
    elif babyhandvalue > dealerhandvalue:
        return '1'
    else:
        return '0.5'

def card_single_char(card):
    if card in numbers:
        return card
    elif card in faces:
        return '0'
    elif card == ace:
        return 'A'

def card_int(card):
    if card in numbers:
        return int(card)
    elif card in faces:
        return 10
    elif card == ace:
        return 11

def convert_hand(cardone, cardtwo):
    cardonevalue = card_int(cardone)
    cardtwovalue = card_int(cardtwo)
    if cardonevalue < cardtwovalue:
        return [cardtwo, cardone]
    else:
        return [cardone, cardtwo]
