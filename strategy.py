# -*- coding: utf-8 -*-

from evaluate import convert_hand
from evaluate import card_single_char
from evaluate import card_int

class Strategy:
    numbers = ['0','2','3','4','5','6','7','8','9','A']

    def __init__(self):
        self.age = 10000
        self.my_strategy = []
        X = len(self.numbers)
        for x in range(0, X):
            for y in range(x, X):
                for z in range(0, X):
                    onetwothree = self.numbers[x] + self.numbers[y] + self.numbers[z]
                    self.my_strategy.append(onetwothree)
                    self.my_strategy.append(0.5)

    def strategize(self, hands):
        for hand in hands:
            babycardone = hand[0]
            babycardtwo = hand[1]
            a = babycardone + babycardtwo
            asorted = ''.join(sorted(a))
            dealercardone = hand[2]
            babybabydealer = asorted + dealercardone

            handindex = self.my_strategy.index(babybabydealer)
            hitpercentindex = handindex + 1
            hitpercent = self.my_strategy[hitpercentindex]

            stand = 0
            hit = 1
            action = int(hand[3])
 
            loss = 0
            win = 1
            result = hand[4]
            if result != '0.5':
                result = int(result)
                increment = (1000 / self.age)
                change = 0
    
                if result == loss and action == stand:
                    change = 1
                elif result == loss and action == hit:
                    change = -1
                elif result == win and action == stand:
                    change = -1
                elif result == win and action == hit:
                    change = 1
                newhitpercent = hitpercent + (change * increment)
     
                if newhitpercent > 1:
                    newhitpercent = 1
                elif newhitpercent < 0:
                    newhitpercent = 0
                self.my_strategy[hitpercentindex] = newhitpercent
            self.age = self.age + 1

    def get_strategy_for_hand(self, hand):
        handindex = self.my_strategy.index(hand)
        hitpercentindex = handindex + 1
        return self.my_strategy[hitpercentindex]